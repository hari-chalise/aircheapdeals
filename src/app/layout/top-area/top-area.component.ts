import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'top-area',
  templateUrl: './top-area.component.html',
  styleUrls: ['./top-area.component.css']
})
export class TopAreaComponent implements OnInit {

  constructor() { }
  isLoading = false;
  // tslint:disable-next-line: typedef
  ngOnInit() {
  }

  loadData() {
    this.isLoading = true;
    setTimeout(() => {
      this.isLoading = false;
    }, 1500);
  }

}
