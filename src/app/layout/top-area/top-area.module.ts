import { FlightsComponent } from './child/flights/flights.component';
import { ActivitiesComponent } from './child/activities/activities.component';
import { RentalsComponent } from './child/rentals/rentals.component';
import { CarsComponent } from './child/cars/cars.component';
import { HotelsComponent } from './child/hotels/hotels.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopAreaComponent } from './top-area.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
@NgModule({
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    TopAreaComponent,
    HotelsComponent,
    CarsComponent,
    FlightsComponent,
    RentalsComponent,
    ActivitiesComponent,
  ],
  exports: [HotelsComponent, TopAreaComponent]
})
export class TopAreaModule { }
