import { TopAreaModule } from './layout/top-area/top-area.module';
import { LastMinuteDealsComponent } from './layout/last-minute-deals/last-minute-deals.component';
import { RecentReviewsComponent } from './layout/recent-reviews/recent-reviews.component';
import { TopDestinationsComponent } from './layout/top-destinations/top-destinations.component';
import { BestPriceComponent } from './layout/best-price/best-price.component';
// import { TopAreaComponent } from './layout/top-area/top-area.component';
import { TopHeaderComponent } from './layout/top-header/top-header.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
// import { CarouselModule } from 'ngx-owl-carousel-o';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FooterComponent } from './layout/footer/footer.component';
// import { OwlModule } from 'ngx-owl-carousel';

@NgModule({
  declarations: [
    AppComponent,
    TopHeaderComponent,
    // TopAreaComponent,
    BestPriceComponent,
    TopDestinationsComponent,
    RecentReviewsComponent,
    LastMinuteDealsComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    TopAreaModule,
    BrowserAnimationsModule ,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
